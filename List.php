<!doctype html>
<html lang="en">
<meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <link rel="stylesheet" href="style.css">
   
</head>
</head>
<body>
<div class="main">
    <?php
    session_start();

    $faculties = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");


    if ($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['navigateToRegister'])) {
        header("Location:complete.php");
    }

    ?>
    <div class="wrapper">
        <div class="form-search">
            <form>
                <div class="field">
                    <label for="faculty" class="field__label">Phân khoa</label>
                    <select name="faculty">
                        <option value="0" selected></option>
                        <?php
                        foreach($faculties as $x => $x_value) {
                            echo '<option value="' . $x_value . '">' . $x_value . '</option>';
                        }
                        ?>
                    </select>
                </div>

                <div class="field">
                    <label for="search" class="field__label">Từ khóa</label>
                    <input type="text" name="search" id="search" class="field__input"/>
                </div>

                <div class="button">
                    <button type="submit" class="btn-submit">Tìm kiếm</button>
                </div>
            </form>
        </div>

        <div class="result">
            <div>
                <h4>Số sinh viên tìm thấy: xxx</h4>
            </div>

            <div>
                <form method="post">
                    <input type="submit" class="btn-add" name="navigateToRegister" value="Thêm" />
                </form>
            </div>
        </div>

        <div class="list-student">
            <table>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Nguyễn Văn A</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <button class="btn-action">Xóa</button>
                        <button class="btn-action">Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td>2</td>
                    <td>Trần Thị B</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <button class="btn-action">Xóa</button>
                        <button class="btn-action">Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td>3</td>
                    <td>Nguyễn Hoàng C</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <button class="btn-action">Xóa</button>
                        <button class="btn-action">Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td>4</td>
                    <td>Đinh Quang D</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <button class="btn-action">Xóa</button>
                        <button class="btn-action">Sửa</button>
                    </td>
                </tr>
            </table>
        </div>

    </div>

</div>



</body>

</html>
