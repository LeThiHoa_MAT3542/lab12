<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <link rel="stylesheet" href="style.css">
   
</head>
<body>

    <?php
        $genders = array("Nữ", "Nam");
        $faculties = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");

        // Create a directory
        if (!file_exists('upload')) {
            mkdir('upload', 0777, true);
        }

        // Delete directory
        // array_map('unlink', glob("upload/*.*"));
        // rmdir("upload");

        // Get String of date-time
        function getStringOfDate() {
            $date   = new DateTime(); //this returns the current date time
            $result = $date->format('Y-m-d-H-i-s');
            $krr    = explode('-', $result);
            $result = implode("", $krr);
            return $result;
        }
    ?>

    <div class="main">
        <div class="wrapper">

            <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    $check = 0;

                    if(empty($_POST["name"])) {
                        echo '<span class="validation">Hãy nhập Họ tên</span> <br>';
                        $check++;
                    }

                    if(empty($_POST["gender"])) {
                        echo '<span class="validation">Hãy chọn Giới tính</span> <br>';
                        $check++;
                    }

                    if(empty($_POST["faculty"])) {
                        echo '<span class="validation">Hãy chọn Phân khoa</span> <br>';
                        $check++;
                    }

                    if(empty($_POST["birthday"])) {
                        echo '<span class="validation">Hãy nhập Ngày sinh</span> <br>';
                        $check++;

                    } else {
                        $date = explode("/",$_POST['birthday']);
                        if(!checkdate($date[1] ,$date[0] ,$date[2]))
                        {
                            echo '<span class="validation">Hãy nhập Ngày sinh đúng định dạng</span> <br>';
                            $check++;
                        }
                    }

                    if(!empty($_FILES["avatar"])) {
                        $acceptable = array(
                            'image/jpeg',
                            'image/png'
                        );

                        if(!in_array($_FILES['avatar']['type'], $acceptable) && (!empty($_FILES["avatar"]["type"]))) {
                            echo '<span class="validation">Hãy tải Hình ảnh lên đúng định dạng JPEG hoặc PNG</span> <br>';
                            $check++;
                        }
                    }

                    if($check == 0) {
                        session_start();
                        $_SESSION = $_POST; // Save information to Session

                        $filename = $_FILES["avatar"]["tmp_name"]; // Get file

                        $originalName = pathinfo($_FILES["avatar"]["name"], PATHINFO_FILENAME); // Get original file name
                        $extension = pathinfo($_FILES["avatar"]["name"], PATHINFO_EXTENSION); // Get extension name of file

                        $destination = "upload/" . $originalName . "_" . getStringOfDate() . "." . $extension; // Path to save image
                        move_uploaded_file($filename, $destination); // Save uploaded picture in your directory

                        $_SESSION['avatarUrl'] = $destination;
                    }

                    if(isset($_SESSION)) {
                        header("Location:conform.php");
                    }

                }
            ?>
            <br>

            <form action="" method="post" enctype="multipart/form-data">
                <div class="field">
                    <label for="name" class="field__label">Họ và tên *</label>
                    <input type="text" name="name" class="field__input"/>
                </div>

                <div class="field">
                    <label class="field__label">Giới tính *</label>
                    <div class="gender">
                    <?php
                    $Gender = array("Nam", "Nữ");
                    $keys = array_keys($Gender);
                    for ($i = 0; $i <= count($Gender) - 1; $i++) { ?>
                        <input type="radio" name="gender" checked="<?php echo "checked"; ?>" value=" <?php echo $Gender[$i]; ?>"> <?php echo $Gender[$i]; ?>
                    <?php } ?>
                    </div>
                    
                </div>

                <div class="field">
                    <label for="faculty" class="field__label">Phân khoa *</label>
                    <select name="faculty">
                        <option value="0" selected disabled></option>
                        <?php
                            foreach($faculties as $x => $x_value) {
                                echo '<option value="' . $x . '">' . $x_value . '</option>';
                            }
                        ?>
                    </select>
                </div>

                <div class="field">
                    <label for="birthday" class="field__label">Ngày sinh *</label>
                    <input name="birthday" class="date form-control field__input" type="text" style="padding: 22px 12px;">
                </div>

                <div class="field">
                    <label for="address" class="field__label">Địa chỉ</label>
                    <input type="text" name="address" class="field__input"/>
                </div>

                <div class="field">
                    <label for="avatar" class="field__label">Hình ảnh</label>
                    <input type="file" name="avatar" id="avatar" class="field__input"/>
                </div>

                <div class="button">
                    <button type="submit" class="btn-submit">Đăng ký</button>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript">  
        $('.date').datepicker({
           format: 'dd/mm/yyyy'
         });
    </script>
</body>

</html>
