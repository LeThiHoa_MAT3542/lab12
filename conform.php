<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <link rel="stylesheet" href="style.css">
   

</head>
<body>
    <div class="main">
        <div class="wrapper">
            <?php
                session_start();
//                print_r($_SESSION);
                include 'path/database.php';

                if ($_SERVER["REQUEST_METHOD"] == "POST") {
                    // Create connection
                    $conn = OpenCon();
                    // echo "Connected Successfully";

                    // Prepare and bind
                    $stmt = $conn->prepare("INSERT INTO student (name, gender, faculty, birthday, address, avatar) VALUES (?, ?, ?, ?, ?, ?)");
                    $stmt->bind_param("sissss", $name, $gender, $faculty, $birthday, $address, $avatar);

                    // Set parameters
                    $name = $_SESSION["name"];
                    $gender = $_SESSION["gender"] - 1;
                    $faculty = $_SESSION["faculty"];
                    $birthday = str_replace('/', '-', $_SESSION["birthday"]);
                    $birthday = date('Y-m-d', strtotime($birthday));
                    $address = $_SESSION["address"];
                    $avatar = $_SESSION["avatarUrl"];
                    // Execute
                    $stmt->execute();
                    $stmt-> store_result();
                    $stmt->close();
                    $conn->close();

                    header("Location:complete.php");

                }
            ?>
            <form method="POST">
                <div class="field">
                    <label for="name" class="field__label">Họ và tên</label>
                    <div>
                        <?php
                            if($_SESSION["name"]) {
                                echo '<span>' . $_SESSION["name"] . '</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label class="field__label">Giới tính</label>
                    <div>
                        <?php
                            if($_SESSION["gender"] == 0) {
                                echo '<span>Nam</span>';
                            } else if($_SESSION["gender"] == 1) {
                                echo '<span>Nữ</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label for="faculties" class="field__label">Phân khoa</label>
                    <div>
                        <?php
                            if($_SESSION["faculty"] == 'MAT') {
                                echo '<span>Khoa học máy tính</span>';
                            } else if($_SESSION["faculty"] == 'KDL') {
                                echo '<span>Khoa học vật liệu</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label for="bithday" class="field__label">Ngày sinh</label>
                    <div>
                        <?php
                            if($_SESSION["birthday"]) {
                                echo '<span>' . $_SESSION["birthday"] . '</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label for="address" class="field__label">Địa chỉ</label>
                    <div>
                        <?php
                            if($_SESSION["address"]) {
                                echo '<span>' . $_SESSION["address"] . '</span>';
                            }
                        ?>
                    </div>
                </div>

                <div class="field">
                    <label for="avatar" class="field__label">Hình ảnh</label>
                    <div>
                        <?php
                            if($_SESSION["avatarUrl"]) {
                                echo '<img src="'. $_SESSION["avatarUrl"] .'" alt="Avatar" width="150" height="100">';
                            }
                        ?>
                    </div>
                </div>

                <div class="button">
                    <button type="submit" class="btn-submit">Xác nhận</button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
